import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  url: string = environment.api;

  constructor(
    private http: HttpClient,
  ) { }

  
  getHeaders() {
    return new HttpHeaders({
      Accept: "application/json"
    });
  }

  getHeadersData() {
    return new HttpHeaders({
      "content-type": "multipart/form-data",

    });
  }


  get(endpoint: string, reqOpts?: any) {
    return this.http.get(this.url + '/' + endpoint, reqOpts).toPromise();
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.url + '/' + endpoint, body, reqOpts).toPromise();
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts).toPromise();
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts).toPromise();
  }

}
