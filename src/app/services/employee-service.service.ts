import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiServiceService } from './api-service.service';
import { Employee } from '../Models/Employee';
@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  selected_employee:Employee=null;
  URL = `employee`;
  

  constructor(private api: ApiServiceService) { }

  getEmployees(search:string) {
    return new Promise(async (resolve, reject) => {
      let headers = this.api.getHeaders();
   

        this.api.get(`${this.URL}/all/${search}`, { headers })
        .then((data: any) => {
          resolve(data);
        })
        .catch(err => {
          // reject(err)
        });
    
      
    });
  }

  deleteEmployees(id:number) {
    return new Promise(async (resolve, reject) => {
      let headers = this.api.getHeaders();
      this.api.delete(`${this.URL}/${id}`, { headers })
        .then((data: any) => {
          resolve(data);
        })
        .catch(err => {
          // reject(err)
        });
    });
  }

  editEmployee(employe: Employee) {    
    return new Promise(async (resolve, reject) => {
     console.log(employe)
      let headers = this.api.getHeaders();
      const formData = new FormData();
      formData.append('image', employe.picture);
      employe.picture="image";
      var employee1=JSON.stringify(employe)
      console.log(employee1)
      formData.append('emp', employee1);
      
      this.api.post(`${this.URL}/edit`,formData, { headers })
        .then((data: any) => {
          resolve(data);
        })
        .catch(err => {
          
        });
    });
  }

 
  addEmployee(employe: Employee) {    
    return new Promise(async (resolve, reject) => {
     console.log(employe)
      let headers = this.api.getHeaders();
      const formData = new FormData();
      formData.append('image', employe.picture);
      employe.picture="image";
      var employee1=JSON.stringify(employe)
      formData.append('emp', employee1);
      
      this.api.post(`${this.URL}/add`,formData, { headers })
        .then((data: any) => {
          resolve(data);
        })
        .catch(err => {
          
        });
    });
  }
}
