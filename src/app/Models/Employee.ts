export interface Employee {
    id: number;
    firstName: string;
    lastName: string;
    entrepriseName: string;
    picture: string;
    dateOfEntry: Date;
    entrepriseAddress: string;
 
}