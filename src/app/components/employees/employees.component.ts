import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Employee } from 'src/app/Models/Employee';
import { EmployeeServiceService } from 'src/app/services/employee-service.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  constructor(public employeeServiceService:EmployeeServiceService,public router:Router) { }
 
  employees$ = new BehaviorSubject<Employee[]>([]);
  search:string="";

  ngOnInit() {
    this.getEmployees();
  }

  searchChanged(){

    this.employeeServiceService.getEmployees(this.search).then((data: any) => {
      console.log(data)
      this.employees$.next(data)
    });

    }

  deleteEmployee(id:number){
  
    this.employeeServiceService.deleteEmployees(id).then((data: any) => {
      var temp_employees = this.employees$.value.filter(x => {
        return x.id != id;
      })
      this.employees$.next(temp_employees)
    });
  }

  editEmployee(employee:Employee){
   this.employeeServiceService.selected_employee=employee;
   this.router.navigate(['/add'])
  }

  getEmployees(){
    this.employeeServiceService.getEmployees(this.search).then((data: any) => {
      console.log(data)
      this.employees$.next(data)
    });
  }

}
