import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeServiceService } from 'src/app/services/employee-service.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  constructor(private fb: FormBuilder, public router: Router,public employeeServiceService:EmployeeServiceService) { }
  employeeForm: any;
  file: any = null;
  image:any;

  ngOnInit() {
    if(this.employeeServiceService.selected_employee)
    {
      this.employeeForm = this.fb.group(
        {
          id: new FormControl(this.employeeServiceService.selected_employee.id),
          firstName: new FormControl(this.employeeServiceService.selected_employee.firstName, Validators.required),
          lastName: new FormControl(this.employeeServiceService.selected_employee.lastName, [Validators.required]),
          picture: new FormControl('', ),
          // dateOfEntry: new FormControl(null, Validators.required),
          entrepriseName: new FormControl(this.employeeServiceService.selected_employee.entrepriseName, [Validators.required]),
          entrepriseAddress: new FormControl(this.employeeServiceService.selected_employee.entrepriseAddress, [Validators.required]),
        }
      );
    }

    else{
      this.employeeForm = this.fb.group(
        {
          firstName: new FormControl('', Validators.required),
          lastName: new FormControl('', [Validators.required]),
          picture: new FormControl('', [Validators.required]),
          // dateOfEntry: new FormControl(null, Validators.required),
          entrepriseName: new FormControl('', [Validators.required]),
          entrepriseAddress: new FormControl('', [Validators.required]),
        }
      );
    }
    
  }

  handleFileInput(e: any) {
    var file;
    if (e.target.files.length > 0) {
      this.file = e.target.files[0]
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.file);
   
    reader.onload = (e) => {
      this.image=reader.result;
  }
  reader.onerror = function (error) {
    console.log('Error: ', error);
  };
  
  }

  addEmployee() {
    this.employeeForm.value.picture = this.file;
    console.log(this.employeeForm.value);
     if(this.employeeServiceService.selected_employee)
     this.employeeServiceService.editEmployee(this.employeeForm.value).then((data: any) => {
      this.router.navigate(['/'])  
    });
    else
    this.employeeServiceService.addEmployee(this.employeeForm.value).then((data: any) => {
      this.router.navigate(['/'])  
    }); 
    
  }

  ngOnDestroy(){
    this.employeeServiceService.selected_employee=null;
  } 

}
